<?php  
add_action('after_setup_theme',function(){
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');

});


add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

?>
